const Task = require("../model/Model");

// find all tasks
const getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

// create new task
const createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
};

// find one task
const getOneTask = () => {
	return Task.findOne({}).then(result => {
		return result;
	})
};

// update task by id
const updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}
		result.status = newContent.status;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
};


module.exports = { getAllTasks, createTask, getOneTask, updateTask };