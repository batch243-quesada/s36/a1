const express = require('express');
const router = express.Router();
const taskController = require("../controller/Controller");

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) => {
	taskController.getOneTask(req.param.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
