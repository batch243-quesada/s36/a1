const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./route/Route');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended : true}));

app.use("/tasks", taskRoute);

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.vvd8gml.mongodb.net/s36-activity?retryWrites=true&w=majority")

app.listen(port, () => console.log('Server is running...'));